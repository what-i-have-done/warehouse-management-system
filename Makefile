# set compiler and compile options
CXX = g++
CXXFLAGS = -Wall -g
LDFLAGS =

# set a list of directories
# INCDIR = include
BUILDDIR := build
SRCDIR := src
TARGET := bin/wms

# set the include folder where the .h files reside
CXXFLAGS += -I$(SRCDIR)

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

$(TARGET): $(OBJECTS)
	@echo " Linking ..."
	@echo " $(CXX) $^ -o $(TARGET) "; $(CXX) $^ -o $(TARGET) $(LDFLAGS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp
	@mkdir -p $(BUILDDIR)
	@echo " $(CXX) $(CXXFLAGS) -c -o $@ $<"; $(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	@echo " Cleaning ...";
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

.PHONY: clean
