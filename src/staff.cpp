#include "staff.h"
#include <cstdlib>
#include <algorithm>
#include <fstream>

static const char * STAFFS_FILE = "data/staffs.dat";

Staff::Staff(const string & name, Department dept, const string & passwd)
{
    this->sid = StaffManage::maxsid++;
    this->name = name;
    this->department = dept;
    this->password = passwd;
}

Staff::Staff(int sid, const string & name, Department dept, const string & passwd)
{
    this->sid = sid;
    this->name = name;
    this->department = dept;
    this->password = passwd;
}


void Staff::setName(const string & name)
{
    this->name = name;
}

void Staff::setPassword(const string & passwd)
{
    this->password = passwd;
}

void Staff::setDept(Department dept)
{
    this->department = dept; /* TODO: delete */
}

int Staff::getSid() const
{
    return sid;
}

const string & Staff::getName()
{
    return name;
}

const Staff::Department Staff::getDept()
{
    return department;
}

std::ostream & operator<<(std::ostream & os, Staff & s)
{
    os << s.sid << " " << s.name << " " << s.department;
    return os;
}

std::istream & operator>>(std::istream & in, Staff::Department & dept)
{
    int val;
    if (in >> val)
    {
        switch (val)
        {
            case Staff::INBOUND:
            case Staff::OUTBOUND:
            case Staff::GENERAL:
                dept = Staff::Department(val);
                break;
            default:
                throw std::out_of_range ("Invalid value for type Staff::Department");
        }
    }
    return in;
}

bool Staff::comparePasswd(const string & passwd)
{
    return (this->password == passwd);
}

const string & Staff::getPassword()
{
    return this->password;
}

const char * Staff::deptStr(const Staff::Department value)
{
    static const char* LUT[] = {"INBOUND", "OUTBOUND", "GENERAL" };
    return LUT[static_cast<int>(value)];
}

void Staff::formatOutput()
{
    std::ios_base::fmtflags f( cout.flags() );

    cout.setf(std::ios_base::left, std::ios_base::adjustfield);

    cout.width(6);
    cout << sid;
    cout.width(15);
    cout << name;
    cout.width(8);
    cout << deptStr(department);

    cout.flags( f );
}

int StaffManage::maxsid = 1000;

StaffManage::StaffManage()
{
    index = -1;
    modified = false;
}

bool StaffManage::getModified()
{
    return modified;
}

void StaffManage::addStaff(const Staff & staff)
{
    staffs.push_back(staff);
    modified = true;
}

void StaffManage::saveIndex()
{
    index = iter - staffs.begin();
}

const Staff & StaffManage::getStaff()
{
    static Staff temp = *(staffs.begin() + index);
    return temp;
}

bool StaffManage::seekStaffBySid(int sid)
{
    for (iter = staffs.begin(); iter != staffs.end(); iter++)
    {
        if (iter->getSid() == sid)
            return true;
    }
    cout << "sid not found" << endl;
    return false;
}

void StaffManage::seekStaffsByName(const string & name)
{
    for (iter = staffs.begin(); iter != staffs.end(); iter++)
    {
        if (iter->getName() == name)
            showStaff();
    }
}

void StaffManage::seekStaffsByDept(const Staff::Department dept)
{
    for (iter = staffs.begin(); iter != staffs.end(); iter++)
    {
        if (iter->getDept() == dept)
            showStaff();
    }
}

void StaffManage::showStaff()
{
    iter->formatOutput();
    cout << endl;
}

void StaffManage::modifyPasswordByIndex(const string & passwd)
{
    (staffs.begin() + index)->setPassword(passwd);
    modified = true;
}

void StaffManage::deleteStaff()
{
    if (question_yesno("whether to delete the staff?"))
    {
        staffs.erase(iter);
        modified = true;
    }
}

void StaffManage::deleteStaffBySid(int sid)
{
    seekStaffBySid(sid);
    if (iter != staffs.end())
    {
        deleteStaff();
        modified = true;
    }
}

void StaffManage::deleteStaffsByName(const string & name)
{
    for (iter = staffs.begin(); iter != staffs.end();)
    {
        if (iter->getName() == name)
        {
            showStaff();
            deleteStaff();
            modified = true;
        }
        else
        {
            iter++;
        }
    }
}

void StaffManage::deleteStaffsByDept(const Staff::Department dept)
{
    seekStaffsByDept(dept);
    if (question_yesno("whether to delete all staffs in this department?"))
    {
        for (iter = staffs.begin(); iter != staffs.end();)
        {
            if (iter->getDept() == dept)
            {
                staffs.erase(iter);
                modified = true;
            }
            else
                iter++;
        }
    }
}

void StaffManage::modifyStaff(int info,
        const string & newname, const Staff::Department newdept,
        const string & newpwd)
{
    switch(info)
    {
        case 1:
            iter->setName(newname);
            modified = true;
            break;
        case 2:
            iter->setDept(newdept);
            modified = true;
            break;
        case 3:
            iter->setPassword(newpwd);
            modified = true;
            break;
    }
}

bool Staff::compareStaffBySid(Staff s1, Staff s2)
{
    return (s1.sid < s2.sid); 
}

bool Staff::compareStaffByDept(Staff s1, Staff s2) 
{
    return (s1.department < s2.department); 
}

void StaffManage::showStaffsBySid()
{
    std::sort(staffs.begin(), staffs.end(), Staff::compareStaffBySid);

    for (iter = staffs.begin(); iter != staffs.end(); iter++)
    {
        iter->formatOutput();
        cout << endl;
    }
}

void StaffManage::showStaffsByDept()
{
    std::sort(staffs.begin(), staffs.end(), Staff::compareStaffByDept);

    for (iter = staffs.begin(); iter != staffs.end(); iter++)
    {
        iter->formatOutput();
        cout << endl;
    }
}

bool StaffManage::compareSidPasswd(const int sid, const string & passwd)
{
    seekStaffBySid(sid);
    if (iter != staffs.end())
    {
        if (iter->comparePasswd(passwd))
            return true;
        else
        {
            cout << "sid and password not match" << endl;
            return false;
        }
    }

    return false;
}

void StaffManage::readStaffs()
{
    staffs.clear();
    int sid;
    string name;
    string passwd;
    Staff::Department dept;
    std::ifstream fin;

    fin.open(STAFFS_FILE, std::ios_base::in);
    if (fin.is_open())
    {
        while (fin >> sid >> name >> dept >> passwd)
            staffs.push_back(Staff(sid, name, dept, passwd));
        fin.close();
    }
}

void StaffManage::writeStaffs()
{
    std::ofstream fout;

    fout.open(STAFFS_FILE, std::ios_base::out);
    if (!fout.is_open())
    {
        std::cerr << "can't open " << STAFFS_FILE << " file for output.\n";
        std::exit(EXIT_FAILURE);
    }

    for (iter = staffs.begin(); iter != staffs.end(); iter++)
    {
        fout << *iter << " " << iter->getPassword() << endl;
    }
    fout.close();
}
