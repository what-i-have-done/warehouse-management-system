#include <ctime>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include "goods.h"

static const char * GOODS_FILE = "data/goods.dat";
static const char * QR_FILE = "data/qrcode/";

char * Goods::timenow()
{
    static char buffer[64];
    std::time_t rawtime;
    std::tm * timeinfo;

    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);

    buffer[std::strftime(buffer, 64, "%Y%m%d%H%M%S", timeinfo)] = '\0';

    return buffer;
}

void Goods::generateOrderNumber()
{
    /* std::srand(std::time(NULL)); */
    /* int random = 10 + (std::rand() % (99 - 10 + 1)); // 2 dight random number */
    static int num = 10;
    if (num > 99)
        num = 10;

    orderNumber = string(timenow()) + std::to_string(num++);
}

Goods::Goods(const string & name)
{
    generateOrderNumber();
    this->name = name;
    status = INBOUND;
}

Goods::Goods(const string & num, const string & name, const Goods::Status status)
{
    this->orderNumber = num;
    this->name = name;
    this->status = status;
}

void Goods::setName(string & name)
{
    this->name = name;
}

void Goods::setStatus(Status status)
{
    this->status = status;
}

const string & Goods::getNumber() const
{
    return orderNumber;
}

std::istream & operator>>(std::istream & in, Goods::Status & status)
{
    int val;
    if (in >> val)
    {
        switch (val)
        {
            case Goods::INBOUND:
            case Goods::OUTBOUND:
                status = Goods::Status(val);
                break;
            default:
                throw std::out_of_range ("Invalid value for type Staff::Department");
        }
    }
    return in;
}

std::ostream & operator<<(std::ostream & os, const Goods & goods)
{
    os << goods.orderNumber << " " << goods.name << " " << goods.status;
    return os;
}

bool Goods::compareGoodsByStatus(const Goods g1, const Goods g2)
{
    return (g1.status == g2.status);
}

bool Goods::compareGoodsByNum(const Goods g1, const Goods g2)
{
    return (g1.orderNumber < g2.orderNumber);
}

const char * Goods::stsStr(const Goods::Status value)
{
    static const char* LUT[] = {"INBOUND", "OUTBOUND" };
    return LUT[static_cast<int>(value)];
}

void Goods::formatGoods()
{
    std::ios_base::fmtflags f( cout.flags() );

    cout.setf(std::ios_base::left, std::ios_base::adjustfield);

    cout.width(20);
    cout << orderNumber;
    cout.width(16);
    cout << name;
    cout.width(8);
    cout << stsStr(status);

    cout.flags( f );
}

Warehouse::Warehouse()
{
    modified = false;
}

bool Warehouse::getModified()
{
    return modified;
}

void Warehouse::inbound(const Goods & goods)
{
    using std::strcat;
    char command[512];
    warehouse.push_back(goods);
    const string orderNumber = goods.getNumber();
    strcat(command, "qrencode -o ");
    strcat(command, QR_FILE);
    strcat(command, orderNumber.c_str());
    strcat(command, ".png \"");
    strcat(command, orderNumber.c_str());
    strcat(command, "\"");
    std::system(command);
    modified = true;
}

bool Warehouse::findGoods(const string & orderNumber)
{
    for (iter = warehouse.begin(); iter != warehouse.end(); iter++)
    {
        if (iter->getNumber() == orderNumber)
        {
            return true;
        }
    }
    cout << orderNumber << " not found" << endl;
    return false;
}

void Warehouse::outbound(const string & orderNumber)
{
    findGoods(orderNumber);
    if (iter != warehouse.end())
    {
        iter->setStatus(Goods::OUTBOUND);
    }
}

void Warehouse::showGoods(const string & orderNumber)
{
    findGoods(orderNumber);
    if (iter != warehouse.end())
    {
        iter->formatGoods();
        cout << endl;
    }
}

void Warehouse::showWarehouse()
{
    std::sort(warehouse.begin(), warehouse.end(), Goods::compareGoodsByNum);
    for (iter = warehouse.begin(); iter != warehouse.end(); iter++)
    {
        iter->formatGoods();
        cout << endl;
    }
}

void Warehouse::showWarehouseByStatus()
{
    std::sort(warehouse.begin(), warehouse.end(), Goods::compareGoodsByStatus);

    for (iter = warehouse.begin(); iter != warehouse.end(); iter++)
    {
        iter->formatGoods();
        cout << endl;
    }

}

void Warehouse::readGoods()
{
    warehouse.clear();
    string orderNumber;
    string name;
    Goods::Status status;
    std::ifstream fin;

    fin.open(GOODS_FILE, std::ios_base::in);
    if (fin.is_open())
    {
        while (fin >> orderNumber >> name >> status)
            warehouse.push_back(Goods(orderNumber, name, status));
        fin.close();
    }
}

void Warehouse::writeGoods()
{
    std::ofstream fout;

    fout.open(GOODS_FILE, std::ios_base::out);
    if (!fout.is_open())
    {
        std::cerr << "can't open " << GOODS_FILE << " file for output.\n";
        std::exit(EXIT_FAILURE);
    }

    for (iter = warehouse.begin(); iter != warehouse.end(); iter++)
    {
        fout << *iter << endl;
    }
    fout.close();
}
