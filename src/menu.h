#ifndef MENU_H_
#define MENU_H_

#include <iostream>
#include "staff.h"
#include "goods.h"
#include "utils.h"

using std::string;
using std::cout;
using std::cin;
using std::endl;

#define MAINMENU "\t\tWelcome to Warehouse Manage System\n\n"\
                 "\t\t\t1 admin login\n"\
                 "\t\t\t2 staff login\n"\
                 "\t\t\t3 quit\n"\

#define STAFFMENU "\t\tStaff Management -- Warehouse Manage System\n\n"\
                  "\t\t\t1 add staff information\n"\
                  "\t\t\t2 seek staff information\n"\
                  "\t\t\t3 delete staff information\n"\
                  "\t\t\t4 modfiy staff information\n"\
                  "\t\t\t5 show all staff information\n"\
                  "\t\t\t6 quit\n"\

#define SSTAFFMENU "\t\tStaff Management -- Warehouse Manage System\n\n"\
                  "\t\t\t1 seek staff information\n"\
                  "\t\t\t2 modfiy staff information\n"\
                  "\t\t\t3 quit\n"\

#define WAREHOUSE "\t\t\tWarehouse Management -- Warehouse Manage System\n\n"\
                  "\t\t\t1 inbound\n"\
                  "\t\t\t2 outbound\n"\
                  "\t\t\t3 query goods\n" \
                  "\t\t\t4 show all goods\n"\
                  "\t\t\t5 quit\n"\

#define SEEKSTAFF "\t\tStaff Management -- Warehouse Manage System\n\n" \
                  "\t\t\t1 seek staff by id\n"\
                  "\t\t\t2 seek staff by name\n"\
                  "\t\t\t3 seek staff by department\n"\
                  "\t\t\t4 quit\n"\

#define DELETESTAFF "\t\tStaff Management -- Warehouse Manage System\n\n" \
                  "\t\t\t1 delete staff by id\n"\
                  "\t\t\t2 delete staff by name\n"\
                  "\t\t\t3 delete staff by department\n"\
                  "\t\t\t4 quit\n"\

#define STAFFINFO "\t\tStaff Management -- Warehouse Manage System\n\n" \
                  "\t\t\t1 modify name\n"\
                  "\t\t\t2 modify department\n"\
                  "\t\t\t3 modify password\n"\
                  "\t\t\t4 quit\n"\

#define STAFFSHOW "\t\tStaff Management -- Warehouse Manage System\n\n" \
                  "\t\t\t1 show all staffs\n" \
                  "\t\t\t2 show all staffs by department\n" \
                  "\t\t\t3 quit\n"

#define GOODSSHOW "\t\tWarehouse Management -- Warehouse Manage System\n\n" \
                  "\t\t\t1 show all goods\n" \
                  "\t\t\t2 show all goods by status\n" \
                  "\t\t\t3 quit\n"\

#define FEATURE "\t\tFeature -- Warehouse Manage System\n\n" \
                "\t\t\t1 Warehouse Manage\n" \
                "\t\t\t2 Staff Manage\n" \
                "\t\t\t3 quit\n"\

class Menu
{
protected:
    string menu;
    char minopt;
    char maxopt;
    char option;
    int count;
public:
    Menu(const string & menu, char min, char max);
    virtual void play(StaffManage & staffs, Warehouse & warehouse);
    virtual void setMenu();
    virtual void select(StaffManage & staffs, Warehouse & warehouse);
};

class SeekStaffMenu : public Menu
{
public:
    SeekStaffMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
};

class DeleteStaffMenu : public Menu
{
public:
    DeleteStaffMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
};

class ModifyStaffMenu : public Menu
{
public:
    ModifyStaffMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
};

class ShowStaffMenu : public Menu
{
public:
    ShowStaffMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
};

class StaffMenu : public Menu
{
private:
    SeekStaffMenu seekMenu;
    DeleteStaffMenu deleteMenu;
    ModifyStaffMenu modifyMenu;
    ShowStaffMenu showMenu;
public:
    StaffMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
    void setMenu();
};

class GoodsShowMenu : public Menu
{
public:
    GoodsShowMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
};

class WarehouseMenu : public Menu
{
private:
    GoodsShowMenu showMenu;
public:
    WarehouseMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
};

class FeatureMenu : public Menu
{
private:
    StaffMenu staffMenu;
    WarehouseMenu warehouseMenu;
public:
    FeatureMenu();
    void select(StaffManage & staffs, Warehouse & warehouse);
};

class MainMenu : public Menu
{
private:
    FeatureMenu featureMenu;
public:
    MainMenu();
    void play(StaffManage & staffs, Warehouse & warehouse);
    void select(StaffManage & staffs, Warehouse & warehouse);
};

#endif
