#include "menu.h"
#include <cstdlib>
#include <stdexcept>

using std::system;

static bool IS_ADMIN = false;
static Staff LOGIN;

Menu::Menu(const string & menu, char min, char max) :
    menu(menu), minopt(min), maxopt(max), count(1) {}

void Menu::play(StaffManage & staffs, Warehouse & warehouse)
{
    while (1)
    {
        system("clear");
        cout << menu;
        if (IS_ADMIN)
        {
            cout << "\t\t\t\t\tWelcome admin to log in\n";
        }
        else
        {
            cout << "\t\t\t\t\twelcome " << LOGIN.getSid() << " to log in\n";
        }
        cout << "please enter your option: ";
        cin.get(option);
        eatline();
        if (option < minopt || option > maxopt)
        {
            ++count;
            if (count > 3)
            {
                cout << "try more than 3 times" << endl;
                getkey();
                break;
            }
            cout << "wrong option" << endl;
            getkey();
            continue;
        }
        count = 1;

        if (option == maxopt)
            break;

        select(staffs, warehouse);
    }
}

void Menu::setMenu()
{
}

void Menu::select(StaffManage & staffs, Warehouse & warehouse) {}

MainMenu::MainMenu() : Menu(MAINMENU, '1', '3') {}

void MainMenu::play(StaffManage & staffs, Warehouse & warehouse)
{
    while (1)
    {
        system("clear");
        cout << menu;
        cout << "please enter your option: ";
        cin.get(option);
        eatline();
        if (option < minopt || option > maxopt)
        {
            ++count;
            if (count > 3)
            {
                cout << "try more than 3 times" << endl;
                getkey();
                break;
            }
            cout << "wrong option" << endl;
            getkey();
            continue;
        }
        count = 1;

        if (option == maxopt)
            break;

        select(staffs, warehouse);
    }
}
void MainMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    int sid;
    string password;
    int count = 0;
    switch(option)
    {
        case '1':
            cout << "enter admin password:" << endl;
            while (1)
            {
                ++count;
                if (count > 3)
                    std::exit(EXIT_FAILURE);
                cin >> password;
                eatline();
                if (password == "admin")
                {
                    IS_ADMIN = true;
                    break;
                }
                else
                {
                    cout << "password not match, please try again" << endl;
                }
            }
            count = 0;
            break;
        case '2':
            while (1)
            {
                ++count;
                if (count > 3)
                    std::exit(EXIT_FAILURE);

                cout << "enter staff sid:" << endl;
                cin >> sid;
                eatline();
                cout << "enter staff password:" << endl;
                cin >> password;
                eatline();
                if (staffs.compareSidPasswd(sid, password))
                {
                    break;
                }
            }
            staffs.saveIndex();
            LOGIN = staffs.getStaff();
            count = 0;
            break;
    }
    cout << IS_ADMIN << endl;
    featureMenu.play(staffs, warehouse);
}

void StaffMenu::setMenu()
{
    menu = "\t\tStaff Management -- Warehouse Manage System\n\n" \
            "\t\t\t1 view own info\n" \
            "\t\t\t2 modify password\n" \
            "\t\t\t3 quit\n";
    minopt = '1';
    maxopt = '3'; 
}

StaffMenu::StaffMenu() : Menu(STAFFMENU, '1', '6')
{
}

void StaffMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    string pwd;
    if (!IS_ADMIN)
    {
        string pwd;
        switch(option)
        {
            case '1':
                LOGIN.formatOutput();
                cout << endl;
                getkey();
                break;
            case '2':
                cout << "enter new password:" << endl;
                cin >> pwd;
                eatline();
                staffs.modifyPasswordByIndex(pwd);
                break;
        }
        return;
    }

    string name;
    Staff::Department dept;
    int sid;
    switch(option)
    {
        case '1':
            cout << 1 << endl;
            do
            {
                cout << "enter staff name:" << endl;
                cin >> name;
                eatline();
                try
                {
                    cout << "enter staff department:" << endl;
                    cin >> dept;
                    eatline();
                }
                catch (std::out_of_range & ex)
                {
                    std::cerr << ex.what() << endl;
                    continue;
                }
                cout << "enter staff password:" << endl;
                cin >> pwd;
                eatline();

                staffs.addStaff(Staff(name, dept, pwd));
            } while (question_yesno("coutinue to add? (y/n)"));
            break;
        case '2':
            seekMenu.play(staffs, warehouse);
            break;
        case '3':
            deleteMenu.play(staffs, warehouse);
            break;
        case '4':
            cout << "enter the staff ID that needs to be modified:" << endl;
            cin >> sid;
            eatline();
            staffs.seekStaffBySid(sid);
            modifyMenu.play(staffs, warehouse);
            break;
        case '5':
            showMenu.play(staffs, warehouse);
            break;
        case '6':
            cout << 2 << endl;
            break;
    }
}

SeekStaffMenu::SeekStaffMenu() : Menu(SEEKSTAFF, '1', '4') {}

void SeekStaffMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    int sid;
    string name;
    string pwd;
    Staff::Department dept;
    switch(option)
    {
        case '1':
            cout << "enter the staff ID that needs to be sought:" << endl;
            cin >> sid;
            eatline();
            if (staffs.seekStaffBySid(sid))
                staffs.showStaff();
            getkey();
            break;
        case '2':
            cout << "enter the staff name that needs to be sought:" << endl;
            cin >> name;
            eatline();
            staffs.seekStaffsByName(name);
            getkey();
            break;
        case '3':
            cout << "enter the department that needs to be sought:" << endl;
            cin >> dept;
            eatline();
            staffs.seekStaffsByDept(dept);
            getkey();
            break;
    }
}

DeleteStaffMenu::DeleteStaffMenu() : Menu(DELETESTAFF, '1', '4') {}

void DeleteStaffMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    int sid;
    string name;
    string pwd;
    Staff::Department dept;
    switch(option)
    {
        case '1':
            cout << "enter the staff ID that needs to be deleted:" << endl;
            cin >> sid;
            eatline();
            staffs.deleteStaffBySid(sid);
            getkey();
            break;
        case '2':
            cout << "enter the staff name that needs to be deleted:" << endl;
            cin >> name;
            eatline();
            staffs.deleteStaffsByName(name);
            getkey();
            break;
        case '3':
            cout << "enter the department that needs to be deleted:" << endl;
            cin >> dept;
            eatline();
            staffs.deleteStaffsByDept(dept);
            break;
    }
}

ModifyStaffMenu::ModifyStaffMenu() : Menu(STAFFINFO, '1', '4') {}

void ModifyStaffMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    string name;
    string pwd;
    Staff::Department dept;
    switch(option)
    {
        case '1':
            cout << "enter new name:" << endl;
            cin >> name;
            eatline();
            staffs.modifyStaff(1, name, dept, pwd);
            staffs.showStaff();
            getkey();
            break;
        case '2':
            cout << "enter new department:" << endl;
            cin >> dept;
            eatline();
            staffs.modifyStaff(2, name, dept, pwd);
            staffs.showStaff();
            getkey();
            break;
        case '3':
            cout << "enter new password:" << endl;
            cin >> pwd;
            eatline();
            staffs.modifyStaff(3, name, dept, pwd);
            staffs.showStaff();
            getkey();
            break;
    }
}

ShowStaffMenu::ShowStaffMenu() : Menu(STAFFSHOW, '1', '3') {}

void ShowStaffMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    switch(option)
    {
        case '1':
            staffs.showStaffsBySid();
            getkey();
            break;
        case '2':
            staffs.showStaffsByDept();
            getkey();
            break;
    }
}

WarehouseMenu::WarehouseMenu() : Menu(WAREHOUSE, '1', '5') {}

void WarehouseMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    string input;
    switch(option)
    {
        case '1':
            if (IS_ADMIN || LOGIN.getDept() == Staff::Department::INBOUND)
            {
                do
                {
                    cout << "enter the inbound name:" << endl;
                    cin >> input;
                    eatline();
                    warehouse.inbound(Goods(input));
                } while (question_yesno("coutinue to inbound? (y/n)"));
            }
            else
            {
                cout << "only inbound staffs can operate" << endl;
                getkey();
            }
            break;
        case '2':
            if (IS_ADMIN || LOGIN.getDept() == Staff::Department::OUTBOUND)
            {
                do
                {
                    cout << "enter the order number:" << endl;
                    cin >> input;
                    eatline();
                    warehouse.outbound(input);
                } while (question_yesno("coutinue to outbound? (y/n)"));
            }
            else
            {
                cout << "only outbound staffs can operate" << endl;
                getkey();
            }
            break;
        case '3':
            do
            {
                cout << "enter the order number:" << endl;
                cin >> input;
                eatline();
                warehouse.showGoods(input);
            } while (question_yesno("coutinue to query? (y/n)"));
            break;
        case '4':
            showMenu.play(staffs, warehouse);
            break;
    }
}

GoodsShowMenu::GoodsShowMenu() : Menu(GOODSSHOW, '1', '3') {}

void GoodsShowMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    string input;
    switch(option)
    {
        case '1':
            warehouse.showWarehouse();
            getkey();
            break;
        case '2':
            warehouse.showWarehouseByStatus();
            getkey();
            break;
    }
}

FeatureMenu::FeatureMenu() : Menu(FEATURE, '1', '3') {}

void FeatureMenu::select(StaffManage & staffs, Warehouse & warehouse)
{
    if (!IS_ADMIN)
    {
        staffMenu.setMenu();
    }
    switch(option)
    {
        case '1':
            warehouseMenu.play(staffs, warehouse);
            break;
        case '2':
            staffMenu.play(staffs, warehouse);
            break;
    }
}

