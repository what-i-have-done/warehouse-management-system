#ifndef UTILS_H_
#define UTILS_H_

#include <iostream>

using std::cout;
using std::endl;

inline void eatline() { while (std::cin.get() != '\n') continue; }
inline void getkey()
{
    system("stty -echo");
    system("stty cbreak");
    cout << "press any key to continue" << endl;;
    std::cin.get();
    system("stty -raw");
    system("stty echo");
}

bool question_yesno(std::string const & message);

#endif
