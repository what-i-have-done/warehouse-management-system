#ifndef STAFF_H_
#define STAFF_H_

#include <iostream>
#include <vector>
#include "utils.h"

using std::string;
using std::cout;
using std::endl;

enum Sex {MALE, FEMALE};

class Staff
{
public:
    enum Department {INBOUND, OUTBOUND, GENERAL};
    const char * deptStr(const Department value);
protected:
    int sid;
    string name;
    string password;
    Department department;
public:
    Staff(const string & name = "none",
        Department dept = GENERAL, const string & password = "none");
    Staff(int sid, const string & name,
        Department dep, const string & password);
    void setName(const string & name);
    void setPassword(const string & passwd);
    void setDept(Department dept);
    int getSid() const; /* TODO: delete */
    const string & getName();
    const Department getDept();
    bool comparePasswd(const string & passwd);
    const string & getPassword();
    static bool compareStaffBySid(Staff s1, Staff s2);
    static bool compareStaffByDept(Staff s1, Staff s2);
    void formatOutput();
    friend std::ostream & operator<<(std::ostream & os,
            Staff & s);
    friend std::istream & operator>>(std::istream & in,
            Department & dept);
};

/*
class GeneralStaff : public Staff
{
public:
    GeneralStaff(int sid, string & name,
        Department dept = GENERAL, string password = "\0");
};

class InboundStaff : public Staff
{
public:
    InboundStaff(int sid, string & name,
        Department dept = GENERAL, string password = "\0");
};

class OutboundStaff : public Staff
{
public:
    OutboundStaff(int sid, string & name,
        Department dept = GENERAL, string password = "\0");
};
*/

class StaffManage
{
    std::vector<Staff> staffs;
    std::vector<Staff>::iterator iter;
    size_t index; // 登入员工所在位置
    bool modified;
public:
    StaffManage();
    static int maxsid;
    void saveIndex();
    const Staff & getStaff();
    void modifyPasswordByIndex(const string & passwd);
    void addStaff(const Staff & staff);
    bool seekStaffBySid(int sid);
    void seekStaffsByName(const string & name);
    void seekStaffsByDept(const Staff::Department dept);
    void deleteStaffBySid(int sid);
    void deleteStaffsByName(const string & name);
    void deleteStaffsByDept(const Staff::Department dept);
    void modifyStaff(int info,
        const string & newname, const Staff::Department newdept,
        const string & newpwd);
    void showStaff();
    void deleteStaff();
    void showStaffsBySid();
    void showStaffsByDept();
    bool compareSidPasswd(const int sid, const string & passwd);
    static int getMaxsid();
    void readStaffs();
    void writeStaffs();
    bool getModified();
};

#endif
