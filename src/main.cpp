#include "staff.h"
#include "goods.h"
#include "menu.h"
#include <unistd.h>

using std::cin;

int main()
{
    Warehouse warehouse;
    StaffManage staffs;

    warehouse.readGoods();
    staffs.readStaffs();

    MainMenu menu;
    menu.play(staffs, warehouse);

    if (staffs.getModified() || warehouse.getModified())
    {
        if (question_yesno("whether to save?"))
        {
            warehouse.writeGoods();
            staffs.writeStaffs();
        }
    }
    /*
    warehouse.inbound(Goods("jsdlkfjs"));
    warehouse.inbound(Goods("djslkfjslf"));
    warehouse.inbound(Goods("sdjlskjdfal"));
    warehouse.inbound(Goods("sdfklfjskl"));
    warehouse.inbound(Goods("jiqou2"));

    warehouse.showWarehouse();

    cout << endl;

    warehouse.writeGoods();

    Warehouse temp;
    temp.readGoods();
    temp.showWarehouse();

    string number;
    cin >> number;
    warehouse.showGoods(number);
    warehouse.outbound(number);

    warehouse.showWarehouse();

    staffs.addStaff(Staff("sjfkl", Staff::INBOUND, "1111"));
    staffs.addStaff(Staff("123", Staff::INBOUND, "1111"));
    staffs.addStaff(Staff("djfkls", Staff::OUTBOUND, "1111"));
    staffs.addStaff(Staff("djfkls", Staff::OUTBOUND, "1111"));
    staffs.addStaff(Staff("uioouo", Staff::INBOUND, "1111"));
    staffs.addStaff(Staff("lkju12o3", Staff::OUTBOUND, "1111"));

    staffs.showStaffsBySid();
    cout << endl;
    */

    /*
    staffs.writeStaffs();

    StaffManage temp;
    temp.readStaffs();
    temp.showStaffsBySid();
    */
    /*
    staffs.showStaffsByDept();

    int sid;
    cin >> sid;
    staffs.deleteStaffBySid(sid);
    staffs.showStaffsBySid();
    */

    /*
    StaffMenu menu;
    menu.play(staffs, warehouse);
    */

    /*
    WarehouseMenu menu;
    menu.play(staffs, warehouse);

    */
    return 0;

}
