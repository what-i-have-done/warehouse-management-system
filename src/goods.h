#ifndef GOODS_H_
#define GOODS_H_

#include <iostream>
#include <vector>

using std::string;
using std::cout;
using std::endl;
using std::vector;

class Goods
{
public:
    enum Status {INBOUND, OUTBOUND};
    const char * stsStr(const Status value);
private:
    string orderNumber;
    string name;
    Status status;
    char * timenow();
    void generateOrderNumber();
public:
    Goods(const string & name);
    Goods(const string & num, const string & name, const Status status);
    void setName(string & name);
    void setStatus(Status status);
    void formatGoods();
    const string & getNumber() const;
    static bool compareGoodsByStatus(const Goods g1, const Goods g2);
    static bool compareGoodsByNum(const Goods g1, const Goods g2);
    friend std::ostream &
        operator<<(std::ostream & os, const Goods & goods);
};

class Warehouse
{
    vector<Goods> warehouse;
    vector<Goods>::iterator iter;
    bool modified;
public:
    Warehouse();
    bool getModified();
    void inbound(const Goods & goods);
    void outbound(const string & orderNumber);
    bool findGoods(const string & orderNumber);
    void removeGoods(const string & orderNumber);
    void showGoods(const string & orderNumber);
    void showWarehouse();
    void showWarehouseByStatus();
    void readGoods();
    void writeGoods(); 
};

#endif
