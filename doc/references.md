## How to simulate “Press any key to continue?”

- https://stackoverflow.com/q/21257544/3737970
- http://c-faq.com/osdep/cbreak.html
- http://www.cplusplus.com/forum/beginner/36318/
    + http://www.cplusplus.com/forum/articles/7312/

## Generic menu class for console applications

- https://codereview.stackexchange.com/q/172895/210081 有很好的改进程序的 Tips
    + https://softwareengineering.stackexchange.com/q/133688/223298

## Makefile

- [Writing Make Files](https://www.cs.bu.edu/teaching/cpp/writing-makefiles/)
- https://stackoverflow.com/questions/2481269/how-to-make-a-simple-c-makefile | How to make a SIMPLE C++ Makefile? - Stack Overflow
- https://www.ivofilot.nl/posts/view/19/Using+a+Makefile+for+compiling+your+program | IvoFilot.nl: Using a Makefile for compiling your program
- https://hiltmon.com/blog/2013/07/03/a-simple-c-plus-plus-project-structure/ | A Simple C++ Project Structure - Hiltmon
- https://spin.atomicobject.com/2016/08/26/makefile-c-projects/ | A Super-Simple Makefile for Medium-Sized C/C++ Projects
- http://nuclear.mutantstargoat.com/articles/make/ | Practical Makefiles, by example
- https://www.math.colostate.edu/~yzhou/computer/writemakefile.html#A%20simple%20makefile | Tutorial on writing makefiles
- https://codereview.stackexchange.com/questions/77157/makefile-and-directory-structure | c++ - Makefile and directory structure - Code Review Stack Exchange
- http://open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1204r0.html#abstract | Canonical Project Structure
- https://github.com/kostrahb/Generic-C-Project/blob/master/Makefile | Generic-C-Project/Makefile at master · kostrahb/Generic-C-Project
- https://www.reddit.com/r/C_Programming/comments/3izgic/is_there_a_standard_folder_structure_for/ | Is there a standard folder structure for programming projects? : C_Programming
- https://www.cs.swarthmore.edu/~newhall/unixhelp/c_codestyle.html | cs.swarthmore.edu/~newhall/unixhelp/c_codestyle.html
- https://softwareengineering.stackexchange.com/questions/379202/folder-structure-for-a-c-project | Folder structure for a C project - Software Engineering Stack Exchange

## iterator

- [How do I save the value of a vector iterator?](https://stackoverflow.com/q/22733506/3737970)

    ```cpp
    size_t n = it - mCards.begin();

    auto it = mCards.begin()+n;
    ```

## Format output

* [Restore the state of std::cout after manipulating it](https://stackoverflow.com/q/2273330/3737970)
